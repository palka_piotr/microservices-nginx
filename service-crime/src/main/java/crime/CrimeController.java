package crime;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by piotr on 10/26/16.
 */
@RestController
@RequestMapping("/api/crime")
public class CrimeController {

    @Value("${server.port}")
    private Integer port;

    @RequestMapping
    public String getName() {
        return String.format("/api/crime service on port: %d", port);
    }

}
