package auth;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by piotr on 10/26/16.
 */
@RestController
@RequestMapping("/api/auth")
public class AuthController {

    @RequestMapping
    public String getName() {
        return String.format("/api/auth service");
    }

}
