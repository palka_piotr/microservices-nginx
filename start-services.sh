#!/bin/bash

PWD="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

cd $PWD/service-auth
mvn clean package
nohup java -jar ./target/service-auth-0.1.0.jar &
cd -

cd $PWD/service-crime
mvn clean package
nohup java -jar ./target/service-crime-0.1.0.jar --server.port=8011 &
nohup java -jar ./target/service-crime-0.1.0.jar --server.port=8012 &
nohup java -jar ./target/service-crime-0.1.0.jar --server.port=8013 &
cd -

cd $PWD/service-stats
mvn clean package
nohup java -jar ./target/service-stats-0.1.0.jar &