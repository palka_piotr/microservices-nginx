package stats;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by piotr on 10/26/16.
 */
@RestController
@RequestMapping("/api/stats")
public class StatsController {

    @RequestMapping
    public String getName() throws InterruptedException {
        Thread.sleep(5000);
        return String.format("/api/stats service");
    }

}
