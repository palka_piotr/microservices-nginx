#!/usr/bin/env bash

NGINX_HOME="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

if [ -f $NGINX_HOME/logs/nginx.pid ]
then
    cat $(echo $NGINX_HOME/logs/nginx.pid)
    kill `cat $(echo $NGINX_HOME/logs/nginx.pid)` > /dev/null 2>&1
fi