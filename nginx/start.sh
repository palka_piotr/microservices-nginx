#!/bin/bash

NGINX_HOME="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
IP_ADDR=`ip addr | grep -Eo 'inet (addr:)?([0-9]*\.){3}[0-9]*' | grep -Eo '([0-9]*\.){3}[0-9]*' | grep -v '127.0.0.1'`

echo "Starting nginx on $IP_ADDR"

sed -e "s|"'$NGINX_HOME'"|$NGINX_HOME|g" \
    -e "s|"'$IP_ADDR'"|$IP_ADDR|g" $NGINX_HOME/nginx.conf.template > $NGINX_HOME/nginx.conf

/usr/bin/nginx -c $NGINX_HOME/nginx.conf